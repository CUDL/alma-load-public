from time import sleep
from threading import Thread
import gzip
import json


class GzipJsonFileWriterThread(Thread):

    def __init__(self, json_files_queue, output_file):
        super().__init__()
        self.q = json_files_queue
        self.file = output_file

        self.daemon = True

    def run(self):
        # Dump out everything on the queue.

        # Wait for first item before opening file
        data = self.q.get()
        if data is None:
            return

        with gzip.open(self.file, "a") as f:
            while True:
                # Write json to file
                json_string = json.dumps(data) + "\n"
                json_bytes = json_string.encode('utf-8')
                f.write(json_bytes)

                # Block until next item available
                data = self.q.get()
                if data is None:
                    return


class PlainTextFileWriterThread(Thread):

    def __init__(self, file_queue, output_file):
        super().__init__()
        self.q = file_queue
        self.file = output_file

        self.daemon = True

    def run(self):
        while True:
            # Don't hog the CPU
            sleep(5)
            # It's your moment!
            # Dump out everything on the queue.
            if not self.q.empty():
                with open(self.file, "a") as f:
                    while not self.q.empty():
                        # Write json to file
                        data = self.q.get()
                        data += "\n"
                        f.write(data)
