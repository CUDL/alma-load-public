from threading import Thread
from time import sleep
import requests
from requests.exceptions import ConnectionError as RequestsConnectionError
import re

EXTRACT_PRIMARY_ID = re.compile('users/(.+)$')


class AlmaUserRetrieverThread(Thread):

    def __init__(self, rate, user_url_queue, to_process_queue, not_retrievable_q, api_key):
        super().__init__()
        self.rate = rate
        self.user_url_q = user_url_queue
        self.to_process_q = to_process_queue
        self.not_retrievable_q = not_retrievable_q
        self.api_key = api_key

        self.daemon = True

    def run(self):
        sleep_time = 1.0 / self.rate

        while True:
            sleep(sleep_time)
            if not self.user_url_q.empty():
                base_url = self.user_url_q.get()
                url = base_url + "?format=json&apikey=" + self.api_key
                try:
                    user_req = requests.get(url)
                    if user_req.status_code == requests.codes.ok:
                        user_json = user_req.json()
                        self.to_process_q.put(user_json)
                    else:
                        primary_key_match = EXTRACT_PRIMARY_ID.search(base_url)
                        self.not_retrievable_q.put(primary_key_match.group(1))
                except RequestsConnectionError:
                    primary_key_match = EXTRACT_PRIMARY_ID.search(base_url)
                    self.not_retrievable_q.put(primary_key_match.group(1))
                except Exception:
                    primary_key_match = EXTRACT_PRIMARY_ID.search(base_url)
                    self.not_retrievable_q.put(primary_key_match.group(1))
