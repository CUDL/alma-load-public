from almaload.distribute_json import _distribute, tree_distribute


def test_distribute():
    assert list(_distribute(2, 1)) == [
        (1, 0, 4),
        (0, 0, 2),
        (0, 2, 2),
        (1, 4, 4),
        (0, 4, 2),
        (0, 6, 2)
    ]


def test_tree_distribute():
    levels, nodes = tree_distribute(4, 2)
    assert levels == 2
    assert list(nodes) == [
        (1, 0, 4),
        (0, 0, 2),
        (0, 2, 2)
    ]
