from datetime import date, datetime

import pytest

from almaload.almausers import (
    parse_date, is_expired, additional_identifiers, AdditionalIdentifier,
    IDType, Status, Role, get_roles, get_notes)
from almaload.exceptions import InvalidUserDataError


@pytest.mark.parametrize('value,expected', [
    ('2017-12-13Z', date(2017, 12, 13)),
    ('2001-12-01Z', date(2001, 12, 1))
])
def test_parse_alma_date(value, expected):
    assert parse_date(value) == expected


@pytest.mark.parametrize('invalid_value', [
    '',
    '2017',
    '2017-01',
    '2017-01-02',
    '2017-01-02Z ',
    ' 2017-01-02Z',
])
def test_parse_alma_date_raises_invalid_user_exception(invalid_value):
    with pytest.raises(InvalidUserDataError):
        parse_date(invalid_value)


@pytest.mark.parametrize('invalid_value', [
    datetime(2017, 1, 1),
    '2017-01-01'
])
def test_is_expired_rejects_non_date_current_date_values(invalid_value):
    with pytest.raises(ValueError):
        is_expired({}, current_date=invalid_value)


@pytest.mark.parametrize('expiration,now,expected', [
    ('2017-06-12Z', date(2017, 6, 11), False),
    ('2017-06-12Z', date(2017, 6, 12), False),
    ('2017-06-12Z', date(2017, 6, 13), True),

    ('1980-01-31Z', date(1980, 1, 30), False),
    ('1980-01-31Z', date(1980, 1, 31), False),
    ('1980-01-31Z', date(1980, 2, 1), True)
])
def test_is_expired(expiration, now, expected):
    user = {'expiry_date': expiration}
    assert is_expired(user, current_date=now) == expected


@pytest.mark.parametrize('default', [True, False])
def test_is_expired_uses_default_for_users_without_expiration(default):
    assert is_expired({}, default=default) == default


def test_is_expired_default_default_is_not_expired():
    assert is_expired({}) is False


def test_is_expired_default_must_be_bool():
    with pytest.raises(ValueError):
        is_expired({}, default=1)


@pytest.fixture()
def user_identifier_1():
    return {
        'user_identifier': [
            {
                'segment_type': 'External',
                'id_type': {
                    'value': 'INST_ID',
                    'desc': 'Institution ID'
                },
                'value': 'FOOP0D0X',
                'status': 'ACTIVE'
            }
        ]
    }


def test_additional_identifiers_1(user_identifier_1):
    ids = list(additional_identifiers(user_identifier_1))

    assert [
        AdditionalIdentifier(
            IDType.INST_ID, 'FOOP0D0X', Status.ACTIVE,
            user_identifier_1['user_identifier'][0], 0)
    ] == ids


@pytest.fixture()
def user_identifier_2():
    return {
        'user_identifier': [
            {
                'segment_type': 'External',
                'id_type': {
                    'value': '02'
                },
                'value': 'Value1',
                'status': 'ACTIVE'
            },
            {
                'segment_type': 'External',
                'id_type': {
                    'value': '02'
                },
                'value': 'Value2',
            },
            {
                'segment_type': 'External',
                'id_type': {
                    'value': '02'
                },
                'value': 'Value3',
                'status': 'INACTIVE'
            },
            {
                'segment_type': 'External',
                'id_type': {
                    'value': 'BARCODE'
                },
                'value': 'Value4',
                'status': 'ACTIVE'
            }
        ]
    }


def test_additional_identifiers_2(user_identifier_2):
    ids = list(additional_identifiers(user_identifier_2))
    id_objects = user_identifier_2['user_identifier']

    assert [
        AdditionalIdentifier(
            IDType.ADDITIONAL_02, 'Value1', Status.ACTIVE,
            id_objects[0], 0),
        AdditionalIdentifier(
            IDType.ADDITIONAL_02, 'Value2', Status.ACTIVE,
            id_objects[1], 1),
        AdditionalIdentifier(
            IDType.ADDITIONAL_02, 'Value3', Status.INACTIVE,
            id_objects[2], 2),
        AdditionalIdentifier(
            IDType.BARCODE, 'Value4', Status.ACTIVE,
            id_objects[3], 3)
    ] == ids



@pytest.fixture()
def patron_with_roles():
    return {
        "user_role": [
            {
                "status": {
                    "value": "ACTIVE"
                },
                "scope": {
                    "desc": "University of Cambridge",
                    "value": "44CAM_INST"
                },
                "role_type": {
                    "value": "200"
                }
            },
            {
                "status": {
                    "value": "INACTIVE",
                    "desc": "Inactive"
                },
                "scope": {
                    "value": "44CAM_INST",
                    "desc": "University of Cambridge"
                },
                "role_type": {
                    "value": "228",
                    "desc": "Collection Inventory Operator"
                },
                "parameter": []
            },
            {
                "status": {
                    "value": "ACTIVE",
                    "desc": "Active"
                },
                "scope": {
                    "value": "44CAM_INST",
                    "desc": "University of Cambridge"
                },
                "role_type": {
                    "value": "220",
                    "desc": "Designs Analytics"
                },
                "parameter": []
            },
            {
                "status": {
                    "value": "INACTIVE",
                    "desc": "Inactive"
                },
                "scope": {
                    "value": None,
                    "desc": None
                },
                "role_type": {
                    "value": "231",
                    "desc": "Course Reserves Manager"
                },
                "parameter": []
            },
            {
                "status": {
                    "value": "ACTIVE",
                    "desc": "Active"
                },
                "scope": {
                    "value": "UL",
                    "desc": "University Library"
                },
                "role_type": {
                    "value": "200",
                    "desc": "Patron"
                },
                "parameter": []
            }
        ]
    }


def test_roles(patron_with_roles):
    roles = list(get_roles(patron_with_roles))
    raw_roles = patron_with_roles['user_role']

    assert roles == [
        Role('200', '44CAM_INST', Status.ACTIVE, raw_roles[0], 0),
        Role('228', '44CAM_INST', Status.INACTIVE, raw_roles[1], 1),
        Role('220', '44CAM_INST', Status.ACTIVE, raw_roles[2], 2),
        Role('231', None, Status.INACTIVE, raw_roles[3], 3),
        Role('200', 'UL', Status.ACTIVE, raw_roles[4], 4)
    ]


@pytest.fixture()
def user_with_notes():
    return {
        'user_note': [
            {
                'segment_type': 'Internal',
                'note_type': {
                    'value': 'LIBRARY',
                    'desc': 'Library'
                },
                'note_text': 'Stub record, original Voyager ID 12345, original'
                             ' User Group Code ARC-UG',
                'user_viewable': False,
                'created_by': 'EX_LIBRIS'
            },
            {
                'segment_type': 'Internal',
                'note_type': {
                    'value': 'OTHER',
                    'desc': 'Other'
                },
                'note_text': 'This record was merged with user '
                             '106789-cambrdgedb',
                'user_viewable': False,
                'created_by': 'EX_LIBRIS',
                'created_date': '2017-10-08T05:27:23.933Z'
            }
        ]
    }


def test_get_notes(user_with_notes):
    n1, n2 = get_notes(user_with_notes)

    assert n1.text == ('Stub record, original Voyager ID 12345, original User '
                       'Group Code ARC-UG')
    assert n1.type == 'LIBRARY'
    assert n1.user_viewable == False
    assert n1.creator == 'EX_LIBRIS'
    assert n1.dict is user_with_notes['user_note'][0]
    assert n1.index == 0

    assert n2.text == ('This record was merged with user 106789-cambrdgedb')
    assert n2.type == 'OTHER'
    assert n2.user_viewable == False
    assert n2.creator == 'EX_LIBRIS'
    assert n2.dict is user_with_notes['user_note'][1]
    assert n2.index == 1
