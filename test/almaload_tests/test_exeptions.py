import traceback

from almaload.exceptions import attach_cause


def test_attach_cause():
    cause_err = ValueError('I\'m the cause.')
    err = ValueError('I\'m caused by the cause.')
    assert attach_cause(err, cause_err) is err
    assert err.__cause__ is cause_err

    msg = ''.join(traceback.format_exception(None, err, None))

    assert 'I\'m the cause.' in msg
    assert 'I\'m caused by the cause.' in msg
