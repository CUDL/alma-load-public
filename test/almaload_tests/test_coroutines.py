import pytest
from unittest.mock import sentinel, MagicMock, call

from almaload.coroutines import (pump, partition, apply, PartitionError,
                                 ApplyError, CoroutineError, chain)


def test_pump():
    target = MagicMock()
    pump(range(10), target)

    assert target.mock_calls == [call.send(x) for x in range(10)]


@pytest.mark.parametrize('key_value, targets', [
    ('a', {}),
    (False, {True: None}),
    (True, {False: None})
])
def test_partition_fails_on_undefined_key_values(key_value, targets):
    with pytest.raises(ValueError) as excinfo:
        partition(lambda x: key_value, targets).send(sentinel)

    assert 'No target to dispatch to for key value' in str(excinfo.value)


def test_partition_by_two():
    targets = MagicMock()
    partitioner = partition(lambda i: i % 2, {0: targets.t0, 1: targets.t1})

    pump(range(6), partitioner)

    assert targets.t0.mock_calls == [call.send(x) for x in [0, 2, 4]]
    assert targets.t1.mock_calls == [call.send(x) for x in [1, 3, 5]]


def test_partition_by_four():
    targets = MagicMock()
    partitioner = partition(lambda i: i % 4, {
        0: targets.t0,
        1: targets.t1,
        2: targets.t2,
        3: targets.t3
    })

    pump(range(12), partitioner)

    assert targets.t0.mock_calls == [call.send(x) for x in [0, 4, 8]]
    assert targets.t1.mock_calls == [call.send(x) for x in [1, 5, 9]]
    assert targets.t2.mock_calls == [call.send(x) for x in [2, 6, 10]]
    assert targets.t3.mock_calls == [call.send(x) for x in [3, 7, 11]]


def test_partition_accepts_none_valued_key():
    target = MagicMock()
    partition(lambda x: None, {None: target}).send(sentinel)

    target.send.assert_called_once_with(sentinel)


def test_apply_without_target():
    func = MagicMock()

    pump(range(10), apply(func))

    assert func.mock_calls == [call(x) for x in range(10)]


def test_apply_with_target():
    func = MagicMock(side_effect=lambda x: x * 2)
    target = MagicMock()

    pump(range(10), apply(func, target))

    assert func.mock_calls == [call(x) for x in range(10)]
    assert target.mock_calls == [call.send(x * 2) for x in range(10)]


def test_apply_throws_errors_if_no_error_target_provided():
    err = ValueError('boom')
    func = MagicMock(side_effect=err)

    with pytest.raises(CoroutineError) as excinfo:
        apply(func).send(sentinel)

    assert excinfo.value.__cause__ is err
    assert 'Failed to apply function' in str(excinfo.value)


def test_apply_throws_errors_to_errors_target_if_provided():
    errors = MagicMock()
    err = ValueError('boom')
    func = MagicMock(side_effect=err)

    apply(func, errors=errors).send(sentinel)

    name, args, kwargs = errors.throw.mock_calls[0]
    thrown_err = args[0]

    assert 'Failed to apply function' in str(thrown_err)
    assert isinstance(thrown_err, ApplyError)
    assert thrown_err.__cause__ is err


def test_partition_throws_errors_if_no_error_target_provided():
    err = ValueError('boom')
    key_func = MagicMock(side_effect=err)

    with pytest.raises(PartitionError) as excinfo:
        partition(key_func, {}).send(sentinel)

    assert 'Failed to partition value with key function' in str(excinfo.value)
    assert excinfo.value.__cause__ is err
    assert excinfo.value.value == sentinel
    assert excinfo.value.function == key_func


def test_partition_throws_errors_to_errors_target_if_provided():
    errors = MagicMock()
    err = ValueError('boom')
    key_func = MagicMock(side_effect=err)

    partition(key_func, {}, errors=errors).send(sentinel)

    name, args, kwargs = errors.throw.mock_calls[0]
    thrown_err = args[0]

    assert 'Failed to partition value with key function' in str(thrown_err)
    assert thrown_err.__cause__ is err
    assert thrown_err.value == sentinel
    assert thrown_err.function == key_func


def test_chain_nothing_returns_target():
    assert chain(target=sentinel.target) is sentinel.target


def test_chain_1():
    a = MagicMock(return_value=sentinel.a)

    assert chain(a, target=sentinel) == sentinel.a

    a.assert_called_once_with(sentinel)


def test_chain_2():
    a = MagicMock(return_value=sentinel.a)
    b = MagicMock(return_value=sentinel.b)

    assert chain(a, b, target=sentinel) == sentinel.a

    a.assert_called_once_with(sentinel.b)
    b.assert_called_once_with(sentinel)


def test_chain_3():
    a = MagicMock(return_value=sentinel.a)
    b = MagicMock(return_value=sentinel.b)
    c = MagicMock(return_value=sentinel.c)

    assert chain(a, b, c, target=sentinel) == sentinel.a

    a.assert_called_once_with(sentinel.b)
    b.assert_called_once_with(sentinel.c)
    c.assert_called_once_with(sentinel)
