import pytest

from almaload.keyconstants import KeyConstant


@pytest.fixture()
def test_keys():
    return KeyConstant.create([
        'foo',
        ('bar', [
            'bing',
            'bong'
        ]),
        ('abc', 'AbC', [
            ('def_', 'def')
        ]),
        ('baz', 'bAz')
    ])


def test_key_lookup(test_keys):
    assert ~test_keys.foo == 'foo'
    assert ~test_keys.bar == 'bar'
    assert ~test_keys.bar.bing == 'bing'
    assert ~test_keys.bar.bong == 'bong'
    assert ~test_keys.abc == 'AbC'
    assert ~test_keys.abc.def_ == 'def'
    assert ~test_keys.baz == 'bAz'


def test_accessing_missing_key_at_root_fails_(test_keys):
    with pytest.raises(AttributeError) as excinfo:
        test_keys.xyz

    assert str(excinfo.value) == '<root> has no key: xyz'


def test_accessing_missing_sub_key_fails_(test_keys):
    with pytest.raises(AttributeError) as excinfo:
        test_keys.bar.bing.boom

    assert str(excinfo.value) == 'bar.bing has no key: boom'
