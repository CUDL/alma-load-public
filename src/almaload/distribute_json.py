"""
Usage: distribute-json (to-zip|to-dir) [options] [<json-stream>] <dest>
       distribute-json [--version|--help]

Options:
    --per-level=<count>, -p=<count>
        The max number of subdirs/files to output in a single directory.
        [Default: 100]

    --input-count=<n>

    --dir-template=<template>
        A template to render in order to produce directory names.

    --file-template=<template>
        A template to render in order to produce file names.

    --use-stream-index
        Use the 'stream_index' property of each JSON object rather than its
        actual position in the stream to index output files.s

    --force,-f
        Overwrite existing zip files.

    --traceback
        Print a traceback on usage errors.

    --help
        Show this help.

    --version
        Print the version and exit
"""

from collections import namedtuple
from functools import reduce
from itertools import takewhile
import json
import math
from pathlib import Path, PurePath
import sys
import tempfile
import traceback
import zipfile

import docopt

from ._version import __version__
from .cmdline import get_input, CmdlineError, parse_input_count


class Node(namedtuple('Node', 'level start size')):
    __slots__ = ()

    def __contains__(self, item):
        if isinstance(item, Node):
            return item.start in self and item.end in self

        return self.start <= item < self.limit

    @property
    def end(self):
        return (self.start + self.size) - 1

    @property
    def limit(self):
        return self.start + self.size

    @property
    def is_leaf(self):
        return self.level == 0


def tree_distribute(total, per_level=100):
    level_count = math.ceil(math.log(total, per_level))

    nodes = takewhile(
            lambda n: n.start < total,
            _distribute(per_level, level_count - 1))

    return (level_count, nodes)


def _distribute(per_level, level, offset=0):
    """

    Arguments:
        per_level:
        level: 0-indexed level, where 0 are the leaf nodes
        offset:
    """
    for x in range(per_level):
        size = per_level**(level + 1)
        start = offset + size * x
        yield Node(level, start, size)
        if level > 0:
            yield from _distribute(per_level, level - 1, offset=start)


default_dir_template = '{first:0{comma_pad},}-{dir_max:0{comma_pad},}'
default_file_template = '{index:0{comma_pad},}.json'


def zip_dir_nodes(items, per_level, total=None, get_index=None):
    """
    Yields: (nodes, objects)
            Where nodes is a list of nodes describing the path to the object,
            and objects is an iterable of (index, obj) pairs.
    """
    if total is None:
        total = len(items)

    items = enumerate(items)
    # remap indices according to get_index
    if get_index is not None:
        items = map(lambda x: (get_index(x[0], x[1]), x[1]), items)
    items = PeekableIterator(items)
    level_count, nodes = tree_distribute(total, per_level)

    path = ()
    try:
        while True:
            next_node = next(nodes)

            while len(path) > 0 and next_node not in path[-1]:
                path = path[:-1]
            path = path + (next_node,)

            if next_node.is_leaf:
                node_items = items.take_while(
                    lambda x: x[0] in next_node)
                yield path, node_items

                if not next(node_items, None) is None:
                    raise ValueError('Caller failed to consume all items')
    except StopIteration as e:
        # ensure all items were consumed
        next_item = next(items, None)
        if next_item is not None:
            raise ValueError(f'item index beyond total encountered; total: '
                             f'{total}, index: {next_item[0]}')
        raise e


class PeekableIterator:
    def __init__(self, iterator):
        self._returned = []
        self._iterator = iter(iterator)

    def take_while(self, predicate):
        for x in self:
            if predicate(x):
                yield x
            else:
                self.push_back(x)
                return

    def push_back(self, x):
        self._returned.append(x)

    def __next__(self):
        if self._returned:
            result = self._returned[-1]
            del self._returned[-1]
            return result
        else:
            return next(self._iterator)

    def __iter__(self):
        return self


def render_dir(node, template, pad=1, comma_pad=1, max_index=float('inf')):
    return template.format(first=node.start, last=node.end,
                           pad=pad, comma_pad=comma_pad,
                           dir_max=min(node.end, max_index))


def render_file(index, template, pad=1, comma_pad=1):
    return template.format(index=index, pad=pad, comma_pad=comma_pad)


def distribute_json(base_dir, json_stream, total, per_level, dir_template=None,
                    file_template=None, get_index=None):
    assert base_dir.is_dir()

    if dir_template is None:
        dir_template = default_dir_template
    if file_template is None:
        file_template = default_file_template

    index_pad = decimal_pad_width(total - 1)
    index_comma_pad = decimal_thousands_separated_pad_width(total - 1)

    for nodes, objects in zip_dir_nodes(json_stream, per_level,
                                        total=total, get_index=get_index):
        created = False
        for index, obj in objects:
            if not created:
                path = reduce(
                    lambda dir, node: dir / render_dir(
                        node, dir_template, pad=index_pad,
                        comma_pad=index_comma_pad, max_index=total - 1),
                    nodes, base_dir)
                path.mkdir(parents=True)
                created = True

            file = path / render_file(index, file_template,
                                      pad=index_pad, comma_pad=index_comma_pad)

            with open(file, 'w') as f:
                json.dump(obj, f, indent=4)


def decimal_pad_width(n):
    return math.ceil(math.log10(n))


def decimal_thousands_separated_pad_width(n):
    dec_width = decimal_pad_width(n)

    return dec_width + math.ceil((dec_width - 3) / 3)


def distribute_json_zipfile(
        zip_archive, json_stream, total, per_level, dir_template=None,
        file_template=None, get_index=None):

    if dir_template is None:
        dir_template = default_dir_template
    if file_template is None:
        file_template = default_file_template

    index_pad = decimal_pad_width(total - 1)
    index_comma_pad = decimal_thousands_separated_pad_width(total - 1)

    for nodes, objects in zip_dir_nodes(json_stream, per_level,
                                        total=total, get_index=get_index):
        created = False
        for index, obj in objects:
            if not created:
                path = PurePath(*map(
                    lambda node: render_dir(
                        node, dir_template, pad=index_pad,
                        comma_pad=index_comma_pad,
                        max_index=total - 1),
                    nodes))
                created = True

            file = path / render_file(index, file_template,
                                      pad=index_pad, comma_pad=index_comma_pad)

            zip_archive.writestr(str(file), json.dumps(obj, indent=4).encode())


def get_stream_index(index, obj):
    return obj['stream_index']


def main():
    args = docopt.docopt(__doc__, version=__version__)

    try:
        json_objects, count = get_input(
            args['<json-stream>'] or '-',
            count_estimate=parse_input_count(args['--input-count']))

        try:
            per_level = int(args['--per-level'])
            if per_level < 2:
                raise CmdlineError('per_level must be at least 2')
        except ValueError as e:
            raise CmdlineError('--per-level value must be an integer') from e

        if count is None:
            raise CmdlineError(
                'Unable to determine number of objects in input stream')

        dest = Path(args['<dest>'])
        get_index = (get_stream_index if args['--use-stream-index'] else None)

        if args['to-dir']:
            if not dest.is_dir():
                raise CmdlineError(
                    f'<dest> is not a directory: {args["<dest>"]}')

            distribute_json(dest, json_objects, count, per_level,
                            dir_template=args['--dir-template'],
                            file_template=args['--file-template'],
                            get_index=get_index)
        else:
            assert args['to-zip']

            if not args['--force'] and dest.exists():
                raise CmdlineError(
                    '<dest> already exists, refusing to overwrite')

            with zipfile.ZipFile(str(dest), mode='w',
                                 compression=zipfile.ZIP_DEFLATED) as zf:
                distribute_json_zipfile(
                    zf, json_objects, count, per_level,
                    dir_template=args['--dir-template'],
                    file_template=args['--file-template'], get_index=get_index)

    except CmdlineError as e:
        print(f'Fatal: {e}', file=sys.stderr)

        if args['--traceback']:
            traceback.print_exception(None, e, e.__traceback__, None,
                                      sys.stderr)
        sys.exit(e.status)


if __name__ == '__main__':
    main()
