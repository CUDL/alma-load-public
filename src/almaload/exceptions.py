
class InvalidUserDataError(ValueError):
    """
    Raised when an actual Alma user object does not correspond to the expected
    structure.
    """
    pass


def attach_cause(exc, cause):
    exc.__cause__ = cause
    return exc


class MigrationWarning(UserWarning):
    pass
