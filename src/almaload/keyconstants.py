
class KeyConstant:
    def __init__(self, parent, name, key, children=()):
        if parent is None and not (name is None and key is None):
            raise ValueError('root nodes cannot have names or keys')

        self.__parent = parent
        self.__name = name
        self.__key = key
        self.__children = {}
        self.add_children(children)

    def is_root(self):
        return (self.__name is None and self.__key is None and
                self.__parent is None)

    def add_children(self, children):
        for child in children:
            name = child.name()
            if name in self.__children:
                raise ValueError(f'Child {name!r} already exists')
            self.__children[name] = child

    def name(self):
        return self.__name

    def key(self):
        return self.__key

    def __invert__(self):
        return self.__key

    def __getattr__(self, item):
        try:
            return self.__children[item]
        except KeyError:
            raise AttributeError(f'{self} has no key: {item}')

    def ancestors_and_self(self):
        """
        Yield the nodes (other than the root) up to and including this node.
        """
        if self.__parent is not None:
            yield from self.__parent.ancestors_and_self()
        if not self.is_root():
            yield self

    def dotted_path(self):
        if self.is_root():
            raise ValueError('root has no path')

        return '.'.join(kc.name() for kc in self.ancestors_and_self())

    def __str__(self):
        if self.is_root():
            return '<root>'
        return self.dotted_path()

    @classmethod
    def root(cls, children=()):
        return KeyConstant(None, None, None, children)

    @classmethod
    def create(cls, items):
        root = cls.root()
        root.add_children(cls.from_item(item, root) for item in items)
        return root

    @classmethod
    def from_item(cls, item, parent):
        if isinstance(item, str):
            name = item
            key = item
            child_items = ()
        elif len(item) == 3:
            name, key, child_items = item
        elif len(item) == 2:
            if all(isinstance(i, str) for i in item):
                name, key = item
                child_items = ()
            else:
                name, child_items = item
                key = name

        kc = KeyConstant(parent, name, key)
        kc.add_children(cls.from_item(child_item, kc)
                        for child_item in child_items)
        return kc
