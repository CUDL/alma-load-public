from collections import namedtuple
import datetime
import enum

from almaload.exceptions import InvalidUserDataError
from almaload.keyconstants import KeyConstant

date_template = '%Y-%m-%dZ'


wrapper_keys = KeyConstant.create([
    'alma_user',
    ('original_ids', [
        'type', 'value', 'is_active'
    ])
])

keys = KeyConstant.create([
    'primary_id',
    'expiry_date',
    ('user_identifier', [
        'segment_type',
        'value',
        'note',
        'status',
        ('id_type', [
            'value',
            'desc'
        ])
    ]),
    ('user_group', [
        'value',
        'desc'
    ]),
    ('user_role', [
        ('status', [
            'desc',
            'value'
        ]),
        ('scope', [
            'desc',
            'value'
        ]),
        ('role_type', [
            'desc',
            'value'
        ]),
        'expiry_date'
    ]),
    ('user_note', [
        'segment_type',
        ('note_type', [
            'value',
            'desc'
        ]),
        'note_text',
        'user_viewable',
        'created_by',
        'created_date'
    ])
])


class SegmentType(enum.Enum):
    EXTERNAL = 'External'
    INTERNAL = 'Internal'


def format_date(date):
    return date.strftime(date_template)


def parse_date(value):
    try:
        time = datetime.datetime.strptime(value, '%Y-%m-%dZ')
    except ValueError as e:
        raise InvalidUserDataError(f'Unable to parse date: {value!r}') from e
    return time.date()


def is_expired(alma_user, current_date=None, default=False):
    """
    Determine if an Alma user expired, based on their overall expiration date.

    Users are considered to expire at the *end* of their expiration date. For
    example, a user expiring on the 5th will be valid throughout the 5th, and
    become expired at midnight on the 6th.

    Note that Alma *dates* have a UTC Z indicator, the current_date, if not
    specified, is the current date in UTC time, not whatever local time is. If
    you specify current_date then you can determine it any way you like of
    course.

    Arguments:
        alma_user: The user to check
        current_date: The date to compare against the user's expiration date
                      to decide if they're expired.
        default: If the user has no expiration date, return this True/False
                 value.
    """
    if default is not True and default is not False:
        raise ValueError('default must be True or False')
    if current_date is None:
        current_date = datetime.datetime.utcnow().date()

    if not type(current_date) == datetime.date:
        raise ValueError(
            f'current_date must be a date; not: {type(current_date)}')

    if 'expiry_date' not in alma_user:
        return default

    return parse_date(alma_user[~keys.expiry_date]) < current_date


class IDType(enum.Enum):
    PRIMARY = '__primary__'
    # Normally 01 is described as a barcode, but inexplicably ExLibris have
    # added another barcode type and removed 01 from our instances.
    ADDITIONAL_02 = '02'
    BARCODE = 'BARCODE'
    INST_ID = 'INST_ID'
    CRSID = 'CRSID'


class Status(enum.Enum):
    ACTIVE = ('ACTIVE', 'Active')
    INACTIVE = ('INACTIVE', 'Inactive')

    def __init__(self, code, desc):
        self.code = code
        self.desc = desc

    @staticmethod
    def for_code(code):
        for status in Status:
            if status.code == code or code is status:
                return status
        raise ValueError(f'No Status has code: {code!r}')


AdditionalIdentifier = namedtuple('AdditionalIdentifier',
                                  'type value status dict index')


def additional_identifiers(alma_user):
    """
    Enumerate the user_identifier values of the alma user.

    Yields: AdditionalIdentifier instances
    """
    uid = keys.user_identifier

    for i, obj in enumerate(alma_user.get(~keys.user_identifier, [])):
        try:
            type = IDType(obj.get(~uid.id_type, {}).get(~uid.id_type.value))
        except ValueError:
            # Ignore IDs we don't have types for (as we'll never access them)
            continue

        yield AdditionalIdentifier(
            type, obj[~uid.value],
            Status.for_code(obj.get(~uid.status, Status.ACTIVE)), obj, i)


class RoleType(enum.Enum):
    USER_MANAGER = ('21', 'User Manager')
    PATRON = ('200', 'Patron')

    def __init__(self, id, desc):
        self.id = id
        self.desc = desc


Role = namedtuple('Role', ['type', 'scope', 'status', 'dict', 'index'])


def get_roles(alma_user):
    """
    Generate the roles held by the specified user.
    """

    for i, role in enumerate(alma_user.get(~keys.user_role, [])):
        status = Status.for_code(role.get(~keys.user_role.status, {})
                                 .get(~keys.user_role.status.value,
                                      Status.ACTIVE))

        scope = (role.get(~keys.user_role.scope, {})
                 .get(~keys.user_role.scope.value))

        type = (role.get(~keys.user_role.role_type, {})
                .get(~keys.user_role.role_type.value))

        yield Role(type, scope, status, role, i)


Note = namedtuple('Note', ['text', 'type', 'user_viewable', 'creator', 'dict', 'index'])


def get_notes(alma_user):
    for i, note in enumerate(alma_user.get(~keys.user_note, [])):
        text = note.get(~keys.user_note.note_text)
        type = (note.get(~keys.user_note.note_type, {})
                .get(~keys.user_note.note_type.value))
        user_viewable = note.get(~keys.user_note.user_viewable)
        creator = note.get(~keys.user_note.created_by)

        yield Note(text, type, user_viewable, creator, note, i)
