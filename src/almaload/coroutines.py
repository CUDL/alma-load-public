from functools import reduce

from almauserutils.jsonstreams import prewarmed

from .exceptions import attach_cause


class CoroutineError(Exception):
    pass


class BaseFunctionCoroutineError(CoroutineError):
    def __init__(self, value, func):
        super().__init__(value, func)

    @property
    def function(self):
        return self.args[1]

    @property
    def value(self):
        return self.args[0]


class PartitionError(BaseFunctionCoroutineError):
    def __str__(self):
        return (f'Failed to partition value with key function: '
                f'{self.function!r}; value: {self.value!r}')


class ApplyError(BaseFunctionCoroutineError):
    def __str__(self):
        return (f'Failed to apply function: '
                f'{self.function!r}; value: {self.value!r}')


def _ensure_target(target):
    """
    Return the devnull() coroutine if target is None.
    """
    return devnull() if target is None else target


def pump(objects, target):
    """
    Push the contents of an iterable into a generator.

    >>> pump(range(3), apply(print))
    0
    1
    2
    """
    for obj in objects:
        target.send(obj)


@prewarmed
def partition(key, targets, errors=None):
    """
    A coroutine which sends received objects to a target based on the result of
    applying a key function.

    Arguments:
        key: A function which returns a key value which corresponds to one of
             the targets.
        targets: A dict of key -> coroutine which objects will be sent to
                 according to their key value.
    Returns: A generator object ready to receive objects via send() calls.

    Example:
        >>> @prewarmed
        ... def log(prefix):
        ...   while True:
        ...     print(prefix, (yield))
        >>> odd_even = partition(lambda x: x % 2 == 0,
        ...                      {True: log('even'), False: log('odd')})
        >>> for x in range(4):
        ...   odd_even.send(x)
        even 0
        odd 1
        even 2
        odd 3
    """
    while True:
        obj = yield
        try:
            key_val = key(obj)
        except Exception as e:
            if errors is None:
                raise PartitionError(obj, key) from e
            errors.throw(attach_cause(PartitionError(obj, key), e))
            continue
        try:
            target = targets[key_val]
        except KeyError:
            raise ValueError(f'No target to dispatch to for key value: '
                             f'{key!r}; targets: {set(targets.keys())}')
        target.send(obj)


@prewarmed
def apply(func, target=None, errors=None):
    """
    A coroutine which sends each received object to a function. If a target
    generator is provided, the function's result is sent to it.

    Example:
        >>> pump(range(3), apply(print))
        0
        1
        2
        >>> pump(range(3), apply(lambda x: x * 2,
        ...                      apply(print)))
        0
        2
        4
    """
    target = _ensure_target(target)
    while True:
        # Don't propagate values thrown into us to errors
        value = yield
        try:
            result = func(value)
        except Exception as e:
            if errors is None:
                raise ApplyError(value, func) from e

            errors.throw(attach_cause(ApplyError(value, func), e))
            continue
        target.send(result)


@prewarmed
def devnull():
    """
    A couroutine which does nothing to objects sent into it.

    Example:
        >>> pump(range(3), devnull())
        >>>
    """
    while True:
        yield


def chain(*coro, target=None):
    """
    Link a series of coroutine-producing functions into a chain, so that
    coro n send()s objects to coro n+1.

    partial() or lambda functions can be used as arguments.

    Arguments:
        *coro: 0 or more functions which take a target coroutine and return a
               coroutine.

    Example:
        >>> from operator import setitem
        >>> from functools import partial
        >>> pump(range(3), chain(
        ...     partial(apply, lambda i: {'num': i}),
        ...     partial(apply, lambda d: setitem(d, 'x', d['num'] * 2) or d),
        ...     target=apply(print)))
        {'num': 0, 'x': 0}
        {'num': 1, 'x': 2}
        {'num': 2, 'x': 4}
    """
    return reduce(lambda a, b: b(a), reversed(coro), target)
