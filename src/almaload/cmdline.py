'''
Transform and filter Alma users to meet Cambridge requirements.

Usage: transform-users [options] <input> <output>

Arguments:
    <input>
        A JSON stream of Alma user objects

    <output>
        A file path, to which a JSON stream of transformed Alma user objects
        will be written. Only objects passing the filter end up here.

Options:
    -e=<file>,--excluded-users=<file>
        Write a JSON stream containing users excluded from the <output> by the
        filter rules. If not provided, excluded users will be silently ignored.

    -f=<file>,--failed-users=<file>
        Write a JSON stream containing users who failed migration. If not
        provided, any user which can't me migrated will abort the process.

    --input-count=<n>
        The expected number of records in the input stream, for the purpose of
        displaying progress. Can be used to display progress when input is not
        a seekable file, or to avoid scanning input file to determine the
        number of records.

    --quiet
        Don't render progress bars.

    --traceback
        Write a stack trace on fatal errors.

    -h,--help
        Show this info.

    -v,--version
        Print the version and exit.

    -n=<n>,--migration-number=<n>
        Which migration to run. [default: 1]

JSON Streams:
    This program reads and writes JSON streams of Alma users. These JSON
    streams are simply serialised JSON objects without any whitespace,
    separated by newlines. (JSON strings can't contain literal newlines, so
    excluding whitespace from the serialisation guarantees documents won't
    contain newlines.)

'''
import sys
import traceback
import warnings
from contextlib import ExitStack, closing

import docopt
import tqdm


from almauserutils import jsonstreams
from ._version import __version__
from .coroutines import apply, prewarmed, pump, devnull
from .exceptions import MigrationWarning


class CmdlineError(Exception):
    def __init__(self, message, status=1):
        self.message = message
        self.status = status


def register_resource(r, exit_stack, wrap=None):
    if exit_stack is not None:
        if wrap is not None:
            exit_stack.enter_context(wrap(r))
        else:
            exit_stack.enter_context(r)
    return r


def open_output_file(path, exit_stack=None):
    if path == '-':
        return register_resource(sys.stdout.buffer, exit_stack)
    try:
        if path.endswith('.bz2'):
            import bz2
            return register_resource(bz2.open(path, 'wb'), exit_stack)
        else:
            return register_resource(open(path, 'wb'), exit_stack)
    except OSError as e:
        raise CmdlineError(f'Unable to open file for writing: {path!r}') from e


def open_input_file(path, exit_stack=None):
    if path == '-':
        return register_resource(sys.stdin.buffer, exit_stack)
    try:
        if path.endswith('.bz2'):
            import bz2
            return register_resource(bz2.open(path, 'rb'), exit_stack)
        else:
            return register_resource(open(path, 'rb'), exit_stack)
    except OSError as e:
        raise CmdlineError(f'Unable to open file for reading: {path!r}') from e


def get_json_output_target(dest, exit_stack=None):
    if dest is None:
        return None

    out_file = open_output_file(dest, exit_stack=exit_stack)

    return register_resource(jsonstreams.dump_incremental(out_file),
                             exit_stack, wrap=closing)


@prewarmed
def add_stream_index(target, start=0):
    """
    A coroutine which counts the number of items that it receives, and adds a
    key to each one with its index.
    """
    i = start
    while True:
        record = yield
        record['stream_index'] = i
        i += 1
        target.send(record)


def stringify_exception(exc):
    """
    Render an exception (and it's linked exceptions) into a string containing
    the traceback and type/message.
    """
    return ''.join(traceback.format_exception(None, exc, exc.__traceback__))


def create_json_error_representation(failed_migration):
    """
    Create the JSON representation for a use that failed migration.
    """
    user_wrapper = failed_migration.user

    user_wrapper['status'] = 'failed'
    user_wrapper['exception'] = stringify_exception(failed_migration)

    return user_wrapper


@prewarmed
def migration_error_handler(target):
    """
    Handle users failing migration by creating a JSON representation of their
    FailedMigrationError for logging.

    Arguments:
        target: The generator to handle the JSON error representations.
    Returns: A coroutine object
    """
    from .initialmigration import FailedMigrationError
    while True:
        try:
            yield
            raise AssertionError('yield did not raise')
        except FailedMigrationError as e:
            target.send(create_json_error_representation(e))


def parse_input_count(value):
    if value is None:
        return None

    try:
        count = int(value)
        if count < 0:
            raise CmdlineError(f'Invalid --input-count: '
                               f'value was negative: {count}')
        return count
    except ValueError as e:
        raise CmdlineError(f'Invalid --input-count: cannot interpret '
                           f'{value!r} as an integer') from None


def estimate_record_count(file):
    count = sum(1 for _ in file)
    file.seek(0)
    return count


def get_input(path, count_estimate=None, exit_stack=None):
    file = open_input_file(path, exit_stack=exit_stack)

    if count_estimate is None and file.seekable():
        count_estimate = estimate_record_count(file)

    return jsonstreams.load(file), count_estimate


class ProgressReporter:
    """
    Maintain a set of 4 progress bars to visualise execution progress. One
    shows overall progress of all records. The other three show the number of
    records that have been directed to the 3 output streams: included
    (successful), excluded and failed.
    """

    # A bit of hacking is required to get tqdm to reliably update the total for
    # each bar. It doesn't seem to expect the total to change dynamically, so
    # changing the total when not changing n won't trigger an update. You could
    # just call refresh() on each change, but that'd be slow.
    #
    # To work around, we re-draw all 4 bars when any one bar is redrawn. tqdm
    # doesn't provide a way to hook into its decisions on when to draw, so we
    # hook its render method to detect draw calls and keep track of which bars
    # have been drawn, then render the others if any were drawn after each
    # update.

    def __init__(self):
        opts = dict(unit=' records', unit_scale=True)
        self.overall_progress = tqdm.tqdm(**opts)
        self.included_progress = tqdm.tqdm(**opts)
        self.excluded_progress = tqdm.tqdm(**opts)
        self.failed_progress = tqdm.tqdm(**opts)

        self._tqs = [self.overall_progress, self.included_progress,
                     self.excluded_progress, self.failed_progress]

        for i, tq in enumerate(self._tqs):
            self._hook_render_calls(i, tq)

        self._renders_performed = [False] * len(self._tqs)

    def _hook_render_calls(self, index, tq):
        """
        Hook the sp() method of the progress bar (which renders the bar) in
        order to detect when a progress bar is rendered.
        """
        orig_sp = tq.sp

        def sp(s):
            self._renders_performed[index] = True
            return orig_sp(s)

        tq.sp = sp

    def maybe_refresh(self):
        """
        refresh() any of our progress bars which weren't rendered if any of
        them were.
        """
        if any(self._renders_performed):
            for i, tq in enumerate(self._tqs):
                if not self._renders_performed[i]:
                    tq.refresh()
                self._renders_performed[i] = False

    def update_progress(self, progress):
        total = progress.total
        seen = progress.total_seen

        self.overall_progress.n = seen
        self.included_progress.n = progress.included_count
        self.excluded_progress.n = progress.excluded_count
        self.failed_progress.n = progress.failed_count

        self.overall_progress.total = total
        self.included_progress.total = seen
        self.excluded_progress.total = seen
        self.failed_progress.total = seen

        # update() calls may trigger a render of the bar
        self.overall_progress.update(0)
        self.overall_progress.update(0)
        self.included_progress.update(0)
        self.excluded_progress.update(0)
        self.failed_progress.update(0)

        # Ensure all bars are rendered if any one bar is - see comments at top
        # of class.
        self.maybe_refresh()


class ProgressState:
    def __init__(self, total):
        self._total = total

        self._included_count = 0
        self._excluded_count = 0
        self._failed_count = 0

        self.listeners = []

    def add_listener(self, listener):
        self.listeners.append(listener)

    def notify_listeners(self):
        for listener in self.listeners:
            listener(self)

    @property
    def total(self):
        if self._total is None:
            return None
        return max(self._total, self.total_seen)

    @total.setter
    def total(self, value):
        self._total = value
        self.notify_listeners()

    @property
    def total_seen(self):
        return self.included_count + self.excluded_count + self.failed_count

    @property
    def included_count(self):
        return self._included_count

    @included_count.setter
    def included_count(self, value):
        self._included_count = value
        self.notify_listeners()

    @property
    def excluded_count(self):
        return self._excluded_count

    @excluded_count.setter
    def excluded_count(self, value):
        self._excluded_count = value
        self.notify_listeners()

    @property
    def failed_count(self):
        return self._failed_count

    @failed_count.setter
    def failed_count(self, value):
        self._failed_count = value
        self.notify_listeners()


def is_progress_enabled(args):
    return (not args['--quiet']) and not any(output == '-' for output in
                                             [args['<output>'],
                                              args['--excluded-users'],
                                              args['--failed-users']])


@prewarmed
def increment_observer(obj, name, target):
    """
    A coroutine which increments a named property on obj when it receives a
    value. Values are sent on to target.
    """
    while True:
        val = yield

        setattr(obj, name, getattr(obj, name) + 1)

        target.send(val)


@prewarmed
def error_increment_observer(obj, name, target):
    """
    A coroutine which increments a property on obj when it catches an
    exception. Exceptions are thrown on to target.
    """
    while True:
        try:
            yield
            raise AssertionError('did  not raise')
        except Exception as e:
            setattr(obj, name, getattr(obj, name) + 1)

            target.throw(e)


def ignore_warnings():
    warnings.simplefilter('ignore', MigrationWarning)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    args = docopt.docopt(__doc__, argv=argv, version=__version__)

    ignore_warnings()
    try:
        with ExitStack() as resources:

            incoming_users, count_estimate = get_input(
                args['<input>'],
                count_estimate=parse_input_count(args['--input-count']))

            migrated_users = get_json_output_target(args['<output>'],
                                                    resources)

            excluded_users = get_json_output_target(args['--excluded-users'],
                                                    resources)

            failed_users = get_json_output_target(args['--failed-users'],
                                                  resources)

            errors = None
            if failed_users is not None:
                errors = migration_error_handler(failed_users)

            progress_reporter = None
            if is_progress_enabled(args):
                progress_reporter = ProgressReporter()
                progress_state = ProgressState(count_estimate)
                progress_state.add_listener(progress_reporter.update_progress)

                migrated_users = increment_observer(
                    progress_state, 'included_count', migrated_users)

                if excluded_users is None:
                    excluded_users = devnull()
                excluded_users = increment_observer(
                    progress_state, 'excluded_count', excluded_users)

                if errors is not None:
                    errors = error_increment_observer(
                        progress_state, 'failed_count', errors)

            from .config import get_config
            from .configdata import all_crsids
            from .initialmigration import InitialMigration
            from .secondmigration import SecondMigration

            config = get_config()
            incoming_fisrt_pass = get_input(args['<input>'], -1)[0]
            crsid_set = all_crsids.from_records(incoming_fisrt_pass)
            config.all_crsids = crsid_set

            migration = SecondMigration if args['--migration-number'] == '2' \
                else InitialMigration
            migrate = migration.from_config(config).migrator

            pump(
                incoming_users,
                apply(
                    migration.ensure_wrapped,
                    add_stream_index(
                        migrate(migrated_users,
                                excluded_users=excluded_users,
                                failed_users=errors))))
    except CmdlineError as e:
        print(f'Fatal: {e}', file=sys.stderr)

        if args['--traceback']:
            traceback.print_exception(None, e, e.__traceback__, None,
                                      sys.stderr)
        sys.exit(e.status)


if __name__ == '__main__':
    main()
