from setuptools import setup, find_packages


def get_version(filename):
    '''
    Parse the value of the __version__ var from a Python source file
    without running/importing the file.
    '''
    import re
    version_pattern = r'^ *__version__ *= *[\'"](\d+\.\d+\.\d+)[\'"] *$'
    match = re.search(version_pattern, open(filename).read(), re.MULTILINE)

    assert match, ('No version found in file: {!r} matching pattern: {!r}'
                   .format(filename, version_pattern))

    return match.group(1)


setup(
    name='alma-load',
    version=get_version('src/almaload/_version.py'),
    packages=find_packages('src'),
    package_dir={'': 'src'},
    author='Phil Jones, Hal Blackburn',
    install_requires=[
        'docopt <1',
        'alma-user-utils >= 0.2.0, < 1',
        'python-dateutil >= 2.6, <3',
        'requests >=2, <3',
        'everett >=0.9, <1',
        'appdirs >=1.4.3, <2',
        'tqdm >=4.19, <5'
    ],
    test_requires=[
        'pytest'
        'pytest-lazy-fixture'
        'pytest-cov'
    ],
    dependency_links=[
        'git+https://bitbucket.org/CUDL/alma-user-utils.git@0.3.0#egg=alma-user-utils-0.3.0'
    ],
    entry_points={
        'console_scripts': [
            'transform-users=almaload.cmdline:main',
            'distribute-json=almaload.distribute_json:main',
        ]
    },
    package_data={
        'almaload': ['data/*'],
    }
)
